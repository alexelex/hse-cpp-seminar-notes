#include <iostream>
#include <cmath>
#include <vector>
#include <syncstream>
#include <thread>

void RacyWorker(int idx) {
    std::cerr << "Thread " << idx << " (id=" << std::this_thread::get_id() << ")" << std::endl;
    // Possible output:
    // Thread 1 (id=Thread 140691653121600)0 (id=140691661514304)
    //
}

void SyncedWorker(int idx) {
    std::osyncstream{std::cerr}
        << "Thread " << idx << " (id=" << std::this_thread::get_id() << ")" << std::endl;
}


void Func(std::mutex* mutex, std::atomic<int>* sum) {
    for (int x = 0; x < 1000; ++x) {
        std::lock_guard guard{*mutex};
        *sum += x;
    }
}

int main() {
    {
        std::cerr << "Racy output:" << std::endl;
        std::jthread t1{RacyWorker, 0};
        std::jthread t2{RacyWorker, 1};
    }

    {
        std::cerr << "Synced output:" << std::endl;
        std::jthread t1{SyncedWorker, 0};
        std::jthread t2{SyncedWorker, 1};
    }

    {
        std::mutex mutex;
        int sum = 0;
        std::cerr << "Racy sum:" << std::endl;
        std::vector<std::thread> threads;
        for (int i = 0; i < 2; ++i) {
            threads.emplace_back([&sum] {
                for (int i = 0; i < 10000; ++i) {
                    sum += std::sqrt(i);
                }
            });
        }

        for (int i = 0; i < 2; ++i) {
            threads[i].join();
        }
        std::cerr << sum << '\n';
    }
}

