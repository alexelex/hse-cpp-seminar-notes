#include <thread>
#include <vector>
#include <iostream>

void Worker(int x, int from, int to, bool* is_prime) {
    for (auto i = std::max(from + 1, 2); i <= to; i++) {
        if (x % i == 0) {
            *is_prime = false;
            return;
        }
    }
}

int main() {
    unsigned int number;

    std::cin >> number;

    auto max_threads = std::thread::hardware_concurrency();
    max_threads = 8;

    bool is_prime = true;

    std::vector<std::thread> workers;

    for (int i = 0; i < max_threads; i++) {
        int from = (number * i) / max_threads;
        int to = std::min((number * (i + 1)) / max_threads, number - 1);

        std::cout << "from " << from << " to " << to << '\n';

        workers.emplace_back(Worker, number, from, to, &is_prime);
    }

    for (int i = 0; i < max_threads; i++) {
        workers[i].join();
    }

    std::cout << "Is prime: " << is_prime << '\n';
}

