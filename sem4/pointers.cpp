#include <string>
#include <iostream>

void Reverse(std::string& str) {
    for (unsigned i = 0; i < str.size() / 2; ++i) {
        char symbol = str[i];
        str[i] = str[str.size() - i - 1];
        str[str.size() - i - 1] = symbol;
    }
}

void ReversePtr(std::string* str) {
    for (unsigned i = 0; i < (*str).size() / 2; ++i) {
        char symbol = (*str)[i];
        (*str)[i] = (*str)[(*str).size() - i - 1];
        (*str)[(*str).size() - i - 1] = symbol;
    }
}

// (*ptr).method() == ptr->method()
// (*ptr).field == ptr->field
// vector[i] == vector.at(i)

void ReversePtr2(std::string* str) {
    for (unsigned i = 0; i < str->size() / 2; ++i) {
        char symbol = str->at(i);
        str->at(i) = str->at(str->size() - i - 1);
        str->at(str->size() - i - 1) = symbol;
    }
}

void Reverse2(std::string& str) {
    for (unsigned i = 0; i < str.size() / 2; ++i) {
        std::swap(str[i], str[str.size() - i - 1]);
    }
}

int main() {
    std::string data = "Avrora";
    std::string other_str = "Stroka";
    std::string one_more_str = "Some text";
    std::string last_str = "Last str";

    Reverse(data);
    std::cout << "data after Reverse: " << data << '\n';

    ReversePtr(&other_str);
    std::cout << "data after ReversePtr: " << other_str << '\n';

    ReversePtr2(&one_more_str);
    std::cout << "data after ReversePtr2: " << one_more_str << '\n';

    Reverse2(last_str);
    std::cout << "data after Reverse2: " << last_str << '\n';

    int a = 1;
    int b = 2;
    int* a_ptr = &a;
    int* b_ptr = &b;

    std::cout << *a_ptr << ' ' << *b_ptr << '\n';
    std::swap(a_ptr, b_ptr);
    std::cout << *a_ptr << ' ' << *b_ptr << '\n';
}

