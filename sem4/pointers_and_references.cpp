#include <iostream>

void Reverse(std::string str) {
    for (unsigned i = 0; i < str.size(); ++i) {
        char symbol = str[i];
        str[i] = str[str.size() - i - 1];
        str[str.size() - i - 1] = symbol;
    }
}

// with pointers
// write here

void ReversePtr(std::string* str) {
    for (unsigned i = 0; i < (*str).size(); ++i) {
        char symbol = (*str)[i];
        (*str)[i] = (*str)[(*str).size() - i - 1];
        (*str)[(*str).size() - i - 1] = symbol;
    }
}



// *. to ->

// std::swap

int DoSmth1(std::vector<int>& v);

int DoSmth2(std::vector<int>* v);

void Func() {
    std::string data;
    auto x = DoSmth1(data);
    auto y = DoSmth2(&data);
    // will data change ?
}


int main() {
    // std::string -> std::string*

    int a = 1;
    int* b = &a; //  &a этоо адрес переменной а

    std::cout << a << '\n';
    std::cout << *b << '\n';

    a = 4;

    std::cout << a << '\n';
    std::cout << *b << '\n';

    int d = a;
    a = 5;

    std::cout << a << '\n';
    std::cout << d << '\n';

    // int* c = static_cast<int*>(5);
    // int* c = (int*)5;

    // std::cout << *c << '\n';

    int* x;
    // int* y = std::nullptr;
    std::cout << "b = " << b << "; x = " << x << '\n';
    x = std::move(b);
    std::cout << "b = " << b << "; x = " << x << '\n';

    std::cout << "x = " << *x << '\n';
    std::cout << "b = " << *b << '\n';
}


