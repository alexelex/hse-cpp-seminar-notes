#include <vector>
#include <array>
#include <string>
#include <iostream>

char GetElement(const std::vector<char>& container, int index) {
    // ..
    index = container.size() - index;
    index %= 2;
    // ..
    return container[index];
}

char GetElement(const std::string& container, int index) {
    // ..
    index = container.size() - index;
    index %= 2;
    // ..
    return container[index];
}

char GetElement(const std::array<char, 5>& container, int index) {
    // ..
    index = container.size() - index;
    index %= 2;
    // ..
    return container[index];
}

template <typename Container>
auto GetElementTemplate(const Container& container, int index) {
    // ..
    index = container.size() - index;
    index %= 2;
    // ..
    return container[index];
}

int main() {
    std::vector<char> vector {'c', 'o', 'n', 't', 'a', 'i', 'n', 'e', 'r'};
    std::string string = "array";
    std::array<char, 5> array {'1', '2', '3', '4', '5'};
    // ...

    std::vector<std::string> book {"first page", "second page", "third page", "last page"};

    std::cout << GetElement(vector, 4) << '\n';
    std::cout << GetElement(string, 4) << '\n';
    std::cout << GetElement(array, 4) << '\n';

    std::cout << GetElementTemplate(vector, 4) << '\n';
    std::cout << GetElementTemplate(string, 4) << '\n';
    std::cout << GetElementTemplate(array, 4) << '\n';
    std::cout << GetElementTemplate(book, 3) << '\n'; // works with char -> auto (line 31)
}

