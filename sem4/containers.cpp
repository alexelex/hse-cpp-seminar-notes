#include <iostream>
#include <iterator>
#include <vector>
#include <set>
#include <unordered_set>

struct Bar {
    int x;
};

struct Foo {
    int x;
    int y;
};

bool operator<(const Foo& first, const Foo& second) {
    if (first.x == second.x) {
        return first.y < second.y;
    }
    return first.x < second.x;
}

int main() {
    std::vector<int> v {3, 1, 4};
 
    auto vi = v.begin();
    std::advance(vi, 2);
    // vi += 2;
    std::cout << *vi << ' ';
 
    vi = v.end();
    std::advance(vi, -2);
    // vi -= 2;
    std::cout << *vi << '\n';

    std::set<Foo> foos;
    foos.insert({1, 2});

    std::set<Bar> bars;
    bars.insert({1});
}


