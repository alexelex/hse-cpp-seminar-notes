struct Foo {
	// public by default
	int def;
private:
	int priv;
protected:
	int prot;
public:
	int pub;
};

class Bar {
	// private by default
	int def;
private:
	int priv;
protected:
	int prot;
public:
	int pub;
};

int main() {
    Foo foo{};
    Bar bar{};

    std::cout << foo.def; // ok
    std::cout << foo.priv; // not ok
    std::cout << foo.prot; // not ok
    std::cout << foo.pub; // ok

    std::cout << bar.def; // not ok
    std::cout << bar.priv; // not ok
    std::cout << bar.prot; // not ok
    std::cout << bar.pub; // ok
}

class Yam : public Bar {
	void Print() const {
		std::cout << def; // not ok
		std::cout << priv; // not ok
		std::cout << prot; // ok
		std::cout << pub; // ok
	}
}

class Yam : protected Bar {
	void Print() const {
		std::cout << def; // not ok
		std::cout << priv; // not ok
		std::cout << prot; // ok
		std::cout << pub; // ok, but now it's protected
	}
}

class Yam : private Bar {
	void Print() const {
		std::cout << def; // not ok
		std::cout << priv; // not ok
		std::cout << prot; // ok, but now it's private
		std::cout << pub; // ok, but now it's private
	}
}
