struct Foo {
	std::string field; // empty

	Foo() = default;

	Foo(const Foo& other)
		: field(other.field)
    {	}

	Foo& operator=(const Foo& other) {
		field = other.field;
	}

    ~Foo() {
        // ...
    }
};


int main() {
    Foo x;
    x.field = "empty";

    auto y = Foo(x);
}

