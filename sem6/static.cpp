#include <iostream>

static int i;  // zero by default
               // dies at the end of the program

void Inc() {
	int counter = 0;
	++counter;
	std::cout << counter << '\n';
}

void StaticInc() {
	static int counter = 0; // работает только в первый раз
	++counter;
	std::cout << counter << '\n';
}

struct Foo {
    static int x;

    Foo() {
        ++x;
    }
};

int Foo::x = 0; // definition

int main() {

    Inc(); // 1
    Inc(); // 1
    Inc(); // 1

    StaticInc(); // 1
    StaticInc(); // 2
    StaticInc(); // 3

    Foo f;
    Foo a{};
    Foo b{};

    std::cout << f.x << "\n";
    std::cout << a.x << "\n";
    std::cout << b.x << "\n";
}

// приют для собак: принимать собаку, взять из приюта, узнать количество собак, получить информацию о всех собаках
// собаки: лаять, кормить, у них есть уникальный id, имена (собаку нельзя переименовать)
