#include <iostream>

struct Foo;
struct Bar;

struct Foo {
    Bar* bar;

    int k;
    bool isEmpty = false;
    bool isNotEmpty{true};
    const int kMaxSize = 1;
};

struct Bar {
    int field;

    void PrintMe() const {
        std::cout << "Foo(field=" << field << ")\n";
    }

    bool operator==(const Foo& other) const {
        return other.field == field;
    }

    void SetField(int new_field) {
        field = new_field;
    }

    int& GetField() const {
        return field;
    }
};

struct Ham {
    std::string name;

    void Print() const;
    std::string Rename(const std::string& str);
    void Resize(int x = 5);
};

void Ham::Print() const {
   // ...
}

struct Person {
	std::string name;
	std::string last_name;

	Person()
		: name("Ivan")
		, last_name("Ivanov")
	{ }

	Person(const std::string& name, const std::string& last_name)
		: name(name)
		, last_name(last_name)
	{
		// конструкторов может быть сколько угодно
	}

	explicit Person(const std::string& name)
		: name(name)
	{ }

	~Person() {
		// сделать что-то перед удалением объекта / для "специального" удаления объекта
	}
};

int main() {
    if (Bar{1} == Bar{2}) {
        // ..
    }

    const Bar bar;
    bar.PrintMe();

    Ham ham;
    ham.Resize();
    ham.Resize(6);
}
