#include <iostream>
#include <vector>

struct Task {};

// constexpr
const int kMaxSize = 100; // dies at the end of the program
std::vector<Task> tasks;  // dies at the end of the program

int global_var;

int kSomething = 100;

static int i;  // zero by default
               // dies at the end of the program

int main() {
    std::cout << kSomething << '\n';
    int kSomething = 10;
    std::cout << kSomething << '\n';

	int local_var;

	for (int i = 0; i < 1; ++i) {
		int other_var;
	}  // other_var dies here

	{
		int last_scope_var;

        // ...
	}  // last_scope_var dies here

	// last_scope_var = 0; // not ok
                           // no such variable

	local_var = 0; // ok
}  // local_var dies here

