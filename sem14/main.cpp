#include <iostream>

// argc -- arguments count
// argv -- vector of arguments

/*
int main(int argc, char* argv[]) {
    std::cout << "argc = " << argc << '\n';
    for (int i = 0; i < argc; ++i) {
        std::cout << argv[i] << '\n';
    }
}
*/

/*
 1. order matters
 2. few arguments without flags (input path, outupt path)
 3. -filter arg1 arg2
 4. validate
*/

class Filter {
    virtual void Apply(Image* img) = 0;
};

class Crop : public Filter {
    Crop(/* ... */);

    void Apply(const Image& img) override;
};

void Crop::Apply() {
    // ...
}

// ...

struct Arguments {
    struct Filter {
        std::string name;
    };

    std::string input_path;
    std::string output_path;

    std::vector<Filter> filters;
};

// Arguments::Filter -> Filter
std::unique<Filter> MakeFilter(const Arguments::Filter& filter) {
    if (filter.name == "crop") {
        return std::make_unique<Crop>(filter.height, filter.width);
    }
    return std::make_unique<Filter2>(...);
}

class Parser {
    Arguments operator()(int argc, char* argv[]);
}

int main(int argc, char* argv[]) {
    Parser parser;
    auto args = parser(argc, argv);

    // ValidateArgument
    // ValidateImage
    // try open output file

    // for each filter: create filter (MakeFilter);
    // for each filter: apply filter
}

class Color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

operator+
operator-
operator*
operator*(const Color& color, float value);
operator*(float valut, const Color& color);

class Image {
private:
    std::vector<std::vector<Color>> colors;
//    int width;
//    int height;
};

class Crop

