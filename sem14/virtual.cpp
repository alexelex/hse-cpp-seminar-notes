#include <iostream>




struct Zoo {
    virtual void Print()  = 0;
};

struct Foo {
    virtual void Print() {
        std::cout << "Foo\n";
    }
};

struct Bar : public Foo {
    void Print() override {
        std::cout << "Bar\n";
    }
};


int main() {
    Foo foo;
    Bar bar;

    foo.Print();
    bar.Print();

    Foo* foo_ptr = new Foo;
    Bar* bar_ptr = new Bar;

    foo_ptr->Print();
    bar_ptr->Print();

    Foo* foo_ptr_to_bar = bar_ptr;
    foo_ptr_to_bar->Print();

    std::cout << sizeof(Foo) << '\n';
    std::cout << sizeof(Bar) << '\n';
    std::cout << sizeof(Zoo) << '\n';

    Zoo* zoo = new Zoo;
}

