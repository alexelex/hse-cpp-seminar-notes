#include <iostream>
#include <fstream>
#include <sstream>

int main() {
    // считывание из строки
    std::istringstream s1("hello");
    std::string content = "aba";
    // char* content.data()

    vector<int> v
    v.data() -> int*

    // new int[100]
    // new char[100]

    size_t size = k byte >= 4 -> at least 4 char
    char* -> 8 byte = 8 char

    12

    s1.read(content.data(), 3);
    std::cout << content;

    // default constructor (input/output stream)
    std::stringstream buf1;
    buf1 << 7;
    int n = 0;
    buf1 >> n;
    std::cout << "buf1 = " << buf1.str() << " n = " << n << '\n';
 
    // input stream
    std::istringstream inbuf("-10");
    inbuf >> n;
    std::cout << "n = " << n << '\n';
 
    // output stream in append mode (C++11)
    std::ostringstream buf2("test", std::ios_base::ate);
    buf2 << '1';
    std::cout << buf2.str() << '\n';
}


