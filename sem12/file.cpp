const std::forward_list<int> a = {1, 2, 3, 4};
auto it = a.begin();
++it++;
std::cout << (*it) << std::endl;
-----
const std::forward_list<int> a = {1, 2, 3, 4};
auto it = a.begin();
++(it++);
std::cout << *it << std::endl;
-----
const std::forward_list<int> a = {1, 2, 3, 4};
auto it = a.begin();
(++it)++;
std::cout << *it << std::endl;


