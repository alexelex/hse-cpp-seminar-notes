struct Node {
    Node* prev;
    Node* next;
    int value;
}

class List {
private:
    // fields

public:
    List();

    void PushBack();
    void PushFront();

    int Back() const;
    int Front() const;

    void PopBack();
    void PopFront();

    ~List();

    class Iterator {
    public:
        explicit Iterator(Node* pointer);

        int& operator*() const;
        int* operator->() const;

        Iterator& operator=(Iterator other);

        Iterator& operator++();
        Iterator operator++(int);
        Iterator& operator--();
        Iterator operator--(int);

        bool operator==(const Iterator& other) const;
        bool operator!=(const Iterator& other) const;
    };
};

Iterator Vector::End() {
    return Iterator(data_ + size_);
}

data_
data_ + 1
...
data_ + (size_ - 1)

// auto end = vector.End();
// std::cout << *end << '\n'; -- UB 

for (auto iter = vector.begin(); iter != vector.end(); ++iter) {
    // ...
}

for (auto elem : vector) {
    
}

