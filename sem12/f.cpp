
struct Threesome {
    int x;
    int y;
    int z;

    std::strong_ordering operator<=>(const Threesome& other) const = default;
};

struct Pair {
    int x;
    int y;
    Threesome three;

    std::strong_ordering operator<=>(const Pair& other) const = default;
    // if Threesome doesn't have operator<=> -> compilation error

    // <=
    // <
    // >
    // >=
    // ==
    // !=

    std::strong_ordering operator<=>(const Pair& other) const {
        // <=
        // <
        // >
        // >=
    }

    bool operator==(const Pair& other) const {
        // ==
        // !=
    }

    bool operator!=(const Pair& other) const {
        // !=
    }
};

struct Date {
    int year;
    int month;
    int day;

    std::strong_ordering operator<=>(const Date& other) const {
        
    }    
};

// return std::strong_ordering::equal;
// x <=> y;
