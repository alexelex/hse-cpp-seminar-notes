#include <iostream>
#include <exception>
#include <stdexcept>
#include <vector>

int Func(int* result) {
    *result = 1;
}

// std::runtime_error
// std::bad_alloc
// std::owerflow_error
// std::range_error

/*
struct {
    Status status;
    Value value;

    bool IsOk() const {
        return status.IsOk();
    }
};
*/

struct MyException
  : public std::exception
{
    const char* what() const throw () {
    	return "C++ Exception";
    }  
};

int FuncThrow() {
    throw 1;
    throw std::runtime_error("");
}

int main() {
//    int result;
//    if (Func(&result) != 0) {
        // .. error
//    }
    // ok

//    Func(&result);

	try {
        while (true) {
            new int[100000000ul];
        } // bad alloc
	} catch (const std::bad_alloc& e) {
		std::cout << "caught alloc\n";
        std::cout << e.what() << '\n';
	} catch (const std::exception& e) {
		std::cout << "caught\n";
	} catch (int x) {
		std::cout << "caught int\n";
	} catch (...) {
		std::cout << "caught anything\n";
	}
}

