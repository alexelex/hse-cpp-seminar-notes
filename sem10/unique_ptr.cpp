#include <iostream>
#include <memory>

//void Func(std::unique_ptr<int> ptr);

//void Func(std::unique_ptr<int>& ptr);

//void Func(int* ptr);

int main() {
    std::unique_ptr<int> ptr = std::make_unique<int>(5);
    std::unique_ptr<int> ptr2(ptr.release());

//    Func(ptr2.get());

    if (!ptr) {
        std::cout << "empty\n";
    }

    ptr.reset();
}
