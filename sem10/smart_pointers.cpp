#include <iostream>
#include <memory>

struct Holder {
    int* ptr;
    int counter;

    ~Holder() {
        --counter;
        if (counter == 0) {
            delete ptr;
        }
    }
};

int main() {
    std::unique_ptr<int> ptr(new int{1});
    std::unique_ptr<int> right = std::make_unique<int>(5);

    std::weak_ptr<int> ptr3;
    {
        std::shared_ptr<int> ptr2 = std::make_shared<int>(6);
        ptr3 = ptr2;
        std::cout << ptr3.use_count() << '\n'; // 1

        auto ptr4 = ptr3.lock();
        if (ptr4) {
            std::cout << *ptr4 << '\n'; // 6
        }

        std::cout << ptr3.use_count() << '\n'; // 2
    } // ptr2 dead -> ptr3 invalid
    std::cout << ptr3.use_count() << '\n'; // 0

    auto ptr5 = ptr3.lock();
    if (!ptr5) {
        std::cout << "already dead\n";
    }
}

