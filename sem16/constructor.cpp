#include <iostream>

struct X {
    X(int x) : x(x) {}
    int x;
    int y;
};

int main() {
    X z = 1;
    std::cout << z.x << '\n';

    auto x = X{.y = 1};
}
