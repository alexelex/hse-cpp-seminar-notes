#include <iostream>
#include <exception>

class Base {
public:
    virtual void Name() = 0;
    virtual ~Base() = default;
};

class Foo : public Base {
public:
    void Name() final {
        std::cout << "Foo\n";
    }
};

class Bar : public Foo {
public:
    void Name() override {
    }
};

int main() {
//    Base f;
    Foo x;
    x.Name();

    try {
        throw 1;
    } catch (std::exception ex) {
        std::cout << "exception\n";
        throw ex;
    } catch (int) {
        std::cout << "int\n";
    }
}

