#include <iostream>

class Alpha {
public:
    void B() const  {
    }

    void A() const {
        B();
    }

    void C() {}
};


int main() {
    Alpha a;
    a.A();

    const Alpha y;
    y.A();
    y.B();
    y.C();
}
