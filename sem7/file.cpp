#include <cassert>
#include <ctime>
#include <iostream>
#include <vector>

/*AdmissionTable FillUniversities(
    const std::vector<University>& universities,
    const std::vector<Applicant>& applicants)
{
    for (const auto& applicant : applicants) {
        // ...
        return {{university, &applicant}};
    }

    const Applicant* = &applicants[0];
    return {};
}*/

enum class Fruits {
    LEMON, // 0
    KIWI   // 1
};
 
enum class Colors {
    PINK, // 0
    GRAY  // 1
};

int main() {
    Fruits fruit = Fruits::LEMON;
    Colors color = Colors::PINK;

    assert(static_cast<int>(fruit) == static_cast<int>(color));

    time_t time1 = 0;
    time_t time2 = 100;

    if (time1 < time2) {
        // ...
    }

    std::time_t result = std::time(nullptr);
    std::cout << result << '\n';
    auto tm = std::localtime(&result);
    std::cout << std::asctime(tm);

    std::string_view str{ "Line" };
    str.remove_prefix(1);
    str.remove_suffix (2);
    std::cout << str << '\n';

    std::vector<int> f{1, 2, 3};
    f.clear();
}


struct State {
    int random_value;
};

// large vector
void Sort(std::vector<int>& data) {
    std::sort(data.beign(), data.end());
}

std::vector<int> Prediction(const State& state) {
    
}

void PrintResult(const std::vector<int>& data);

void Main() {
    State state{42};
    auto pred = Prediction(state);
    Sort(pred);
    PrintResult(pred);
}
