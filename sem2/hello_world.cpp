#include <iostream>
#include <string>
#include <cinttypes> // int64_t / int8_t / uint16_t ...
#include <climits>
#include <limits> // https://en.cppreference.com/w/cpp/types/numeric_limits


int main() {
    char f; // sybmol
    // ascii;  -128 <= char <= 127
    unsigned char e;

    signed short a;
    signed int b;
    long int c;
    long long int d;

    unsigned short ua; // 2 -> 16
    unsigned u; // 4 -> 32
    unsigned long int lu; // 8 byte = 64 bit

    std::cout << "short " << sizeof(a) << "\n";
    std::cout << "unsigned long " << sizeof(lu) << '\n';

    // from climits library
    // INT_MAX
    // ULLONG_MAX

    // int a = -5;
    // unsigned b = 3;

    // 0000..011 = 3
    // 1000...00 = -1
    // 1000...01 = -2

    int y = 0;
    std::cout << y << '\n';  // 0
    std::cout << ++y << '\n';  // 1
    std::cout << y << '\n';  // 1
    std::cout << y++ << '\n';  // 1
    std::cout << y << '\n';  // 2

    // --y and y--

    bool type = false;

    // && -- and
    // || -- or

    if (type) {
        // ...
    } else if (!y) {
        // ... if y == 0
    } else {
        // ...
    }

    while (...) {
        // ...
        break;
    }

    do {
        // ...
    } while ();

    for ( ; ; ) {
        // ..
        break;
    }

    for (int = 3; x > 0; --x) {
        // ...
    }

    for (int x = 0; x < 3; ++x) {
        // ...
    }
}

// comment

/*

comment
comment

*/

