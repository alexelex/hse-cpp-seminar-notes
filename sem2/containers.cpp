#include <iostream>
#include <vector>
#include <array>
#include <string>
#include <unordered_map>
#include <map>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <cctype>
#include <deque>
#include <queue>
#include <stack>


const int kSize = 5;

bool compareFunc(int a, int b) {
    return a < b;
}

bool compareMe(const std::string& a, const std::string& b) {
    return a < b;
}

int main() {
    std::vector<int> a(5); // int int int int int
    std::vector<int> b(5, 0); // 0 0 0 0 0
    std::vector<int> c{0, 1, 2, 3, 4}; // 0 1 2 3 4

    // push_back / emplace_back
    int elem = a[0];
    int one_more = a[1];
    size_t sz = a.size(); //  ~> unsigned int (size_t)
    int signed_size = std::ssize(a);
    c.push_back(7); // c = 0 1 2 3 4 7

    // kSize = 7; // compilation error

//    for (unsigned i = 1; i >= 0; --i) {
//        inf loop
//   }

    std::string s = "I am Alice";
    if (std::isspace(s[0])) {
        // ...
    }

    if (s[0] == 'I') {
        // ...
    }

    std::sort(a.begin(), a.end(), compareFunc);

    auto f = a.begin();
    int first_elem = *f;

    std::vector<std::string> strings;
    std::sort(strings.begin(), strings.end(), [f](const std::string& a, const std::string& b){
        // you can use f
        return a < b;
    });

    std::array<int, 5> arr;
    std::map<int, int> dict; // binary tree
    std::unordered_map<int, int> dict2; // python dict = hash table

    std::set<int> binary_tree;
    std::unordered_set<int, int> python_set;

    std::string s;

    std::queue<int> q;// fifo
    std::deque<int> dq;
    std::stack<int> st// filo
}

struct Student {
    std::string name;
    int age;
};

void foo() {
    std::vector<Student> students;

    Student one;
    one.age = 3;
    if (one.name == "Julia") {
        // ...
    }
}

