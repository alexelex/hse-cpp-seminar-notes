#include <iostream>
#include <unordered_map>
#include <set>

/*
vector
set
queue
stack
map
array
unordered_map
unordered_set
list
forward_list
deque

set-map
unorderd_set-unordered_map
*/
int main() {
    std::set<int> data {6, 1, 2, 3, 9};
    for (int elem : data) {
        std::cout << elem << '\n';
    }

    // begin {*container}
    // ..
    // end

    for (const auto& elem : data) {
        data.emplace_back(elem);
    }


    unordered_map<int, int> map;
    if (map[0]) {
        // ...
    }

    map.insert({1, 2});

    if (map.find(1) != map.end()) {
        // ...
    }

    try {
        if (map.at(2)) {

        }
    } catch (...) {
    }
}
