#include <iostream>

int main() {
    unsigned a, b;

    a = 17; // 10001
    b = 12; //  1100

    // and -- &
    // or -- |
    // xor -- ^
    // left shift -- <<
    // right shift -- >>
    // not -- ~

    std::cout << (a & b) << '\n';
    std::cout << (a | b) << '\n';
    std::cout << (a ^ b) << '\n';
    std::cout << (b << 1) << '\n'; // 24 11000
    std::cout << b << '\n';        // 12  1100
    std::cout << (b >> 1) << '\n'; // 6    110
    std::cout << (1 << 10) << '\n'; // 1024
    std::cout << (~b) << '\n';

    std::cout << (-1 << 1) << '\n'; // ub
}
