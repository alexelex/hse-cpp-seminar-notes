#include <iostream>

struct Date {
    int day;
    int year;
};

int main() {
    Date p{
        .year = 2,
        .day = 1,
    };

//    p.day = 10;
//    p.year = 100;

    std::cout << p.day << ' ' << p.year << '\n';
}

