#include <iostream>

class Foo {
 public:
    Foo(int x) : x(x) {}

 private:
    int x;

 public:
    bool operator<(const Foo& other) const {
        return x < other.x;
    }
};

bool operator>(const Foo& lhs, const Foo& rhs) {
    return lhs.x < rhs.x;
}

int main() {
    Foo f(1);
    Foo g(2);
    if (f < g) {
        std::cout << "ok\n";
    }
}
