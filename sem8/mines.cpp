#include <string>
#include <vector>

class Minesweeper {
private:
    Field* field;

public:
    struct Cell {
        size_t x = 0;
        size_t y = 0;
    };

    enum class GameStatus {
        NOT_STARTED,
        IN_PROGRESS,
        VICTORY,
        DEFEAT,
    };

    using RenderedField = std::vector<std::string>;

    Minesweeper(size_t width, size_t height, size_t mines_count) {
        field = new Field{...};
    }

    Minesweeper(size_t width, size_t height, const std::vector<Cell>& cells_with_mines) {
        field = new Field{...};
    }

    void NewGame(size_t width, size_t height, size_t mines_count) {
        delete field;
        field = nullptr;

        field = new Field{...};
    }

    void NewGame(size_t width, size_t height, const std::vector<Cell>& cells_with_mines) {
        delete field;
        field = new Field{...};
    }

    void OpenCell(const Cell& cell);
    void MarkCell(const Cell& cell);

    GameStatus GetGameStatus() const;
    time_t GetGameTime() const;

    RenderedField RenderField() const;

    ~Minesweeper() {
        delete field;
        field = nullptr;
    }
};


using MinesweeperPtr = *Minesweeper;

MinesweeperPtr InitGame();

void OpenCellCommand(MinesweeperPtr minesweeper);
void MarkCellCommand(MinesweeperPtr minesweeper);
void GetGameStatusCommand(MinesweeperPtr minesweeper);
void GetGameTimeCommand(MinesweeperPtr minesweeper);
void PrintCommand(MinesweeperPtr minesweeper);

int main() {
    MinesweeperPtr minesweeper = InitGame();
    std::string command;

    while (std::cin >> command) {
        if (command == "OpenCell") {
            OpenCellCommand(minesweeper);
        } else if (command == "MarkCell") {
            MarkCellCommand(minesweeper);
        } else if (command == "GetGameStatus") {
            GetGameStatusCommand(minesweeper);
        } else if (command == "GetGameTime") {
            GetGameTimeCommand(minesweeper);
        } else if (command == "Print") {
            PrintCommand(minesweeper);
        } else {
            std::cout << "unexpected command " << command << "\n";
            std::cout << "expected one of (OpenCell, MarkCell, " <<
                << "GetGameStatus, GetGameTime, Print)\n";
        }
    }

    delete minesweeper;
}

void OpenCellCommand(MinesweeperPtr minesweeper) {
    Minesweeper::Cell cell;
    std::cin >> cell.x >> cell.y;
    minesweeper->OpenCell(cell);
}

void MarkCellCommand(MinesweeperPtr minesweeper) {
    Minesweeper::Cell cell;
    std::cin >> cell.x >> cell.y;
    minesweeper->MarkCell(cell);
}

void GetGameStatusCommand(MinesweeperPtr minesweeper) {
    auto status = minesweeper->GetGameStatus();
    if (status == Minesweeper::GameStatus::NOT_STARTED) {
        std::cout << "NOT_STARTED\n";
    } else if (status == Minesweeper::GameStatus::IN_PROGRESS) {
        std::cout << "IN_PROGRESS\n";
    } else if (status == Minesweeper::GameStatus::VICTORY) {
        std::cout << "VICTORY\n";
    } else {
        std::cout << "DEFEAT\n"
    }
}

void GetGameTimeCommand(MinesweeperPtr minesweeper) {
    auto tm = minesweeper->GetGameTime();
    std::cout << tm << "s\n";
}

void PrintCommand(MinesweeperPtr minesweeper) {
    auto field = minesweeper->RenderField();
    for (const auto& line : field) {
        std::cout << line << "\n";
    }
}
