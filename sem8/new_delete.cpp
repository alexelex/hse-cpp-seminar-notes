#include <memory>
#include <iostream>

int* BadFunc() {
    int result = 100;
    return &result;
}

int* GoodFunc() {
    int* result_ptr = new int{100};
    return result_ptr;
}

int main() {
    int* result = BadFunc();
    int* result2 = GoodFunc();

    if (*result2) {
        std::cout << *result2 << '\n';
    }

    if (*result) {
        std::cout << *result << '\n';
    }

    delete[] result2;
}

// result_ptr -> [int int int]

// new -> delete
// new[] -> delete[]
