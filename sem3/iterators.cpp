#include <vector>
#include <cassert>
#include <iostream>
#include <list>
#include <cctype>
#include <unordered_map>

int main() {
    std::vector<int> data{1, 3};
    std::vector<int>::iterator obj = data.begin();
    std::vector<int>::const_iterator obj_const = data.begin();

    assert(*obj == data[0]);

    obj += 1;
    assert(*obj == data[1]);

    obj += 1;

    // obj += 4; -- ok if data.size() >= 6
    assert(obj == data.end());
    *obj;

    for (std::vector<int>::iterator iter = data.begin(); iter != data.end(); ++iter) {
        std::cout << *iter << ' ';
    }

    for (unsigned index = 0; index < data.size(); ++index) {
        std::cout << data[index] << ' ';
    }

    std::cout << '\n';
    for (int iter : data) {
        std::cout << iter << ' ';
    }
    std::cout << '\n';

    std::list<int> l {1, 2, 3, 4, 5};

    for (std::list<int>::iterator iter = l.begin(); iter != l.end(); ++iter) {
        std::cout << *iter <<  ' ';
    }

/*
    InputIterator iter;
    std::cout << *iter << ' '; // ok
    std::cout << *iter << ' '; // not ok
    ++iter;
    std::cout << *iter << ' '; // ok

    ForwardIter iter;
    std::cout << *iter << ' '; // ok
    std::cout << *iter << ' '; // ok
    --iter; // not ok
    ++iter; // ok

    BidirectionalIter iter;
    std::cout << *iter << ' '; // ok
    std::cout << *iter << ' '; // ok
    --iter; // ok
    ++iter; // ok
    iter += 5; // not ok

    RandomAccessIter iter;
    std::cout << *iter << ' '; // ok
    std::cout << *iter << ' '; // ok
    --iter; // ok
    ++iter; // ok
    iter += 5; // ok
*/

    std::string input_str;
    for (char sybmol : input_str) {
        if (std::isspace(symbol)) {
            // ...
        }
    }

    std::unordered_map<int, int> dict;
    std::unordered_map<int, int>::const_iterator result = dict.find(3);
    if (result != dict.end()) {
        // found
        std::cout << (*result).first << ' ' << (*result).second << '\n';
    } else {
        // ooups
    }

/*
    ListNode* start;
    start++; -- iterator wrapper
    start = start->right; -- implementation
*/

    // * and ->
}

/*
struct ListNode {
    left
    right
    value
};
*/

/*

| 1 | 2 | 3 | 4 |

  x   x
  |   |

*/

/*
vec1 = 1 2 3 4 5
vec2 = 6 7 8 9 10

zigzag iterator:

obj (vec1, vec2)
1 6 2 7 ...
*/
