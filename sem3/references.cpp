#include <vector>
#include <cassert>
#include <algorithm>
#include <iostream>

void Sort(std::vector<int>& v) {
    std::sort(v.begin(), v.end());
    assert(v[2] == 3);
    assert(v[3] == 5);
}

bool Compare(char& left, char& right); // 8 byte
bool Compare(char left, char right); // 2 byte

void Print(const std::vector<int>& v) {
    for (unsigned i = 0; i < v.size(); ++i) {
        std::cout << v[i] << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::vector<int> data {1, 2, 5, 3};
    Sort(data);
    assert(data[3] == 5);

    const std::vector<int> const_vector{1, 2, 3};
    // Sort(const_vector);
    Print(const_vector);
    Print(data);
}

